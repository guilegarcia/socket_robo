#define DEBUG true
#define TIMEOUT 5000

int pinSensor = 3;                          //Pino onde esta conectado o sensor magnético.
long rotTotal = 0;
long distTotal = 0;
float RPM, velocidade;
unsigned long startTime, totalTime;
int count;

void setup() {
//Inicialização das portas seriais:
  Serial.begin(115200);
  Serial1.begin(115200);
  Serial.println("Iniciando");
  
  //Comandos iniciais para o esp:
  //sendData("AT+CWMODE=3",TIMEOUT, DEBUG);
  sendData("AT+CWJAP=\"Cardonix\",\"!w7xtcg!\"",TIMEOUT,DEBUG);    //Conecta à rede wifi
  sendData("AT+CIPMUX=0",TIMEOUT,DEBUG);
  sendData("AT+CIPSTART=\"UDP\",\"192.168.1.8\",8888",TIMEOUT,DEBUG);

  startTime = millis();
  count = 0;
}

void loop() {
    if(digitalRead(pinSensor) == HIGH){
    while(digitalRead(pinSensor) == HIGH);
    RPM = count * 10.0;
    velocidade = RPM * 6.283 * 0.26;          //metros percorridos em um minuto
    velocidade *= 60;                         //metros percorridos em uma hora
    velocidade /= 1000;                       //quilometros percorridos por hora
    
    startTime = millis();
    
    //Concatena e envia os dados em forma de string
    String message = "1";
    int lenght = sizeof(message);
    
    sendData("AT+CIPSEND=1",200,DEBUG);   //AT+CIPSEND=TAMANHO,MESSAGEM
    
    sendData(message, 200, DEBUG);

    count = 0;    
  }
/*  if(digitalRead(pinSensor) == HIGH){
    while(digitalRead(pinSensor) == HIGH);
    rotTotal ++;
    distTotal += rotTotal*(6.283 * 0.26)/1000;      //quilometros percorridos (2*pi*R)/1000
    totalTime = millis()/1000;                      //Tempo total de percurso em segundos
    count++;
    
  }*/
}

//Função responsável pelo envio dos comandos AT para o esp8266
//Timeout é o tempo que a porta serial espera por uma resposta,
String sendData (String comando, const int timeout, boolean debug){
  String resposta = "";
  long int time = millis();
  Serial1.println(comando);
  //Serial.println(comando);
  while((time + timeout) > millis()){       // Enquanto o tempo inicial, mais o tempo de espera não for igual ao tempo atual, continua lendo as resposta da porta serial.
    while(Serial1.available()){
      //Le carater por caracter, e vai adicionando-o à resposta final
      char c = Serial1.read();
      resposta += c;
    }
  }
  if(debug){
    Serial.println(resposta);                 //Mostra na tela do usuário a resposta devolvida pelo esp8266
  }
  return resposta;
}
