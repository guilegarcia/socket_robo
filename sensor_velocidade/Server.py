import socket, sys, time

name = input("Nome do usuario: ")
arq = open("%s.txt"%name, "a")
arq.write("Time(s)          Speed(Km/h)\n")
arq.close()

HOST = ''
PORT = 8888

data = ""

try:
     s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
     print("Socket criado ")
except socket.error as msg:
     print ('Failed to create socket')

# bind do socket com o address do servidor
try:
     s.bind((HOST, PORT))
     print("Servidor online")
except socket.error:
     print ('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
     sys.exit()

s.settimeout(1)

startTime = time.time()
totalTime = 0
rotations = 0

while 1:
     if time.time() - startTime > 6:
          totalTime += (time.time() - startTime)
          dist = rotations * 6.28 * 0.26
          speed = dist/6
          speed *= 3.6
          
          arq = open("%s.txt"%name,"a")
          arq.write("%f             %f\n"%(totalTime,speed))
          print("Speed:%.2f Km/h"%speed)
          rotations = 0
          startTime = time.time()
          arq.close()
     try:
          data = s.recv(10)
          if data != '':
               print("Data recebida: %s"%data)
               rotations += 1

     except socket.timeout:
          pass

s.close()
