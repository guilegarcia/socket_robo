import sys
import time

import readchar
from Wheels import Wheels

w = Wheels()
speed = 10  # 0 to 10


def readChar():
    c = str(repr(readchar.readkey()))
    c = c.replace("'", "")
    if c == "\\x1b[A":
        return "up"
    elif c == "\\x1b[C":
        return "right"
    elif c == "\\x1b[D":
        return "left"
    elif c == "\\x1b[B":
        return "down"
    elif c == " ":
        return " "
    elif c == "\\x03":
        return "ctrlC"


while True:
    c = readChar()
    print(c)
    if c == "up":
        w.move(10 + speed)
        if speed < 10:
            speed += 1
    elif c == "right":
        w.move_wheel_right(10)
        w.move_wheel_left(10 + speed)
    elif c == "left":
        w.move_wheel_right(10 + speed)
        w.move_wheel_left(10)
    elif c == "down":
        w.move(10 - speed)
        if speed > -10:
            speed -= 1
    elif c == " ":
        w.move_wheel_right(10)
        w.move_wheel_left(10)
    elif c == "ctrlC":
        sys.exit(1)
    time.sleep(1)
