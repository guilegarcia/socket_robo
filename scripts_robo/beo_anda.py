from Wheels import Wheels
from Audio import Audio
from Eyes import Eyes
from Movements import Movements

audio = Audio()
movements = Movements()
eyes = Eyes()

w = Wheels()
speed = 5  # 0 to 10


def cumprimentar():
    audio.play_mp3('audio/cenario/oi.mp3')
    eyes.set_expression('default')
    movements.play_motion_file('moves/tchau.dat')
    sleep(1)

for x in range(10):
    w.move(10 + speed)
    if speed < 10:
        speed += 1
    # Comprimenta na metade
    if x is 5:
        comprimentar()

