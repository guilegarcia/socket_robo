import sys
sys.path.append("/home/pi/beo/")

from movements import Movements
from audio import Audio
m = Movements()
a = Audio()

a.play_mp3("arturMusic.mp3", volume=0.2)
m.playMotionFile("motions/artur_part1")
m.playMotionFile("motions/artur_part2")
m.playMotionFile("motions/artur_part3")
m.playMotionFile("motions/artur_part4")
m.playMotionFile("motions/artur_part5")
m.playMotionFile("motions/artur_slowmo")

