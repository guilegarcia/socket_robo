import sys
sys.path.append("/home/pi/beo/")
from time import sleep

from audio import Audio
from eyes import Eyes
from movements import Movements
from clicker import Clicker

a = Audio()
e = Eyes()
m = Movements()
c = Clicker()
e.setExpression('default')

motionDir = "/home/pi/beo/cacismPresentation/motions/"

#PART 1
c.wait()
m.playMotionFile(motionDir+"075levantaumamao.dat")
c.wait()
m.playMotionFile(motionDir+"076abaixaumamao.dat")

#PART 2
c.wait()
m.playMotionFile(motionDir+"077levantaduasmaos.dat")

#PART 3
c.wait()
e.setExpression('mad')
m.playMotionFile(motionDir+"078abaixaduasmaos.dat")

#PART 4
c.wait()
e.setExpression('shut')
a.play_mp3("yay.mp3")
m.playMotionFile(motionDir+"079beocomemora.dat")
e.setExpression('default')


#PART 5
c.wait()
a.play_mp3("078eu.mp3")
m.playMotionFile(motionDir+"078eu.dat")
e.setExpression('wink')

#PART 5.1
c.wait()
m.playMotionFile(motionDir+"078_1olha.dat")


#PART 6
c.wait()
e.setExpression('default')
a.play_mp3("079osalunos.mp3")
m.playMotionFile(motionDir+"079osalunos.dat")
m.playMotionFile(motionDir+"079_1.alemda.dat")

#PART 7
c.wait()
a.play_mp3("080temosum.mp3")
m.playMotionFile(motionDir+"080temosum.dat")
a.play_mp3("081elestem.mp3")
m.playMotionFile(motionDir+"081elestem.dat")

#PART 8
c.wait()
a.play_mp3("082Ishall.mp3")
m.playMotionFile(motionDir+"082Ishall.dat")

#PART 9
c.wait()
a.play_mp3("083yohablo.mp3")
m.playMotionFile(motionDir+"083yohablo.dat")


#PART 10
c.wait()
a.play_mp3("084jeparle.mp3")
m.playMotionFile(motionDir+"084jeparle.dat")
a.play_mp3("085ichspreche.mp3")
m.playMotionFile(motionDir+"085ichspreche.dat")
a.play_mp3("086equalquer.mp3")
m.playMotionFile(motionDir+"086equalquer.dat")

#PART 11
c.wait()
a.play_mp3("087poderia.mp3")
m.playMotionFile(motionDir+"087poderia.dat")
a.play_mp3("088masteu.mp3")
sleep(2.5)
a.play_mp3("arturMusic.mp3", volume = 0.2)
e.setExpression("shut")
sleep(1)
m.playMotionFile(motionDir+"artur_part1")
sleep(0.1)
m.playMotionFile(motionDir+"artur_part2")
sleep(0.1)
m.playMotionFile(motionDir+"artur_part3")
sleep(0.1)
m.playMotionFile(motionDir+"artur_part4")
sleep(0.1)
m.playMotionFile(motionDir+"artur_part5")
e.setExpression('wink')
sleep(0.1)
m.playMotionFile(motionDir+"artur_slowmo")
e.setExpression('default')

#PART 12
c.wait()
a.play_mp3("089foimeu.mp3")
m.playMotionFile(motionDir+"089foimeu.dat")
#sleep(3)
a.play_mp3("090nosfizemos.mp3")
m.playMotionFile(motionDir+"090nosfizemos.dat")
#sleep(2)
a.play_mp3("091eleme.mp3")
m.playMotionFile(motionDir+"091eleme.dat")

#PART 13
c.wait()
a.play_mp3("092nao.mp3")
m.playMotionFile(motionDir+"092nao.dat")
a.play_mp3("093ocontato.mp3")
m.playMotionFile(motionDir+"093ocontato.dat")
a.play_mp3("094euajudo.mp3")
m.playMotionFile(motionDir+"094euajudo.dat")
a.play_mp3("095elesadoram.mp3")
m.playMotionFile(motionDir+"095elesadoram.dat")
a.play_mp3("096eeuadoro.mp3")
m.playMotionFile(motionDir+"096eeuadoro.dat")

#PART 14
c.wait()
a.play_mp3("097euconsigo.mp3")
m.playMotionFile(motionDir+"097euconsigo.dat")
a.play_mp3("098sentirtoques.mp3")
m.playMotionFile(motionDir+"098sentirtoques.dat")
a.play_mp3("099medirdistancia.mp3")
m.playMotionFile(motionDir+"099medirdistancia.dat")
a.play_mp3("100andar.mp3")
sleep(0.7)
a.play_mp3("101controlarTV.mp3")
m.playMotionFile(motionDir+"101controlarTV.dat")
a.play_mp3("102meconectar.mp3")
m.playMotionFile(motionDir+"102meconectar.dat")
a.play_mp3("103enfim.mp3")
m.playMotionFile(motionDir+"103enfim.dat")
a.play_mp3("104soumuito.mp3")
m.playMotionFile(motionDir+"104soumuito.dat")
e.setExpression("wink")
#PART 15
c.wait()

