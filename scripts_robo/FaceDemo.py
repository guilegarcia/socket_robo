#!/usr/bin/env python

import eyes
import camera
import movements
import ultrasonic
import audio
import sensorTouch
from movements.pyxl320 import XL320Class
from time import sleep
from random import randint, random, choice

serialComm = XL320Class.XL320()
e = eyes.Eyes()
m = movements.Movements(serialComm)
f = camera.FaceDetector()
u = ultrasonic.Ultrasonic()
a = audio.Audio()
t = sensorTouch.SensorTouch(serialComm)

e.setExpression('default')
m.disableAllJoints()

goal = [0 for i in range(9)]
wink = 0
count = 0
count2 = 0
count3 = 0
div = 15.0
shouldTestTouch = True
while True:
    count = (count + 1) % 1000
    if count == 999:
        m.disableAllJoints()

    if count2 > 40:
        for i in range(3):
            m.disableJoint(i*2)

    for i in range(3):
        angle = m.getAngle(i*2)
        degree = m.angleToDegree(i*2, angle)
        m.setAngle(i*2 +1,degree)

    for i in range(6,9):
        error = (m.jointsCenter[i]+goal[i]) - m.goalPos[i]
        m.setRawAngle(i, m.goalPos[i] + error/div)

    if count2 % 13 == 0:
        d = u.getDistance()
        print 'd',d
        if d < 70.0:
            print 'Sonar!'
            m.playMotionFile('abana.dat')

    if False: #count2 % 3 == 0:
        count3 = count3 + 1
        t.updateStatus()
        touch = t.getStatus()
        if touch == 1 and shouldTestTouch:
            shouldTestTouch = False
            count3 = 0
            a.play_mp3('carinho.mp3')
            e.setExpression('shut')
        elif count3 == 5:
            shouldTestTouch = True
            e.setExpression('default')

    if count2 % 40 == 0:
        faces = f.detect()
        if len(faces) > 0:
            print 'faces[0]', faces[0]
            pos = camera.rect2centerpos(faces[0])
            print 'pos', pos
            face_yaw, face_pitch = \
                    camera.pixel2angle(pos)
            print 'angles', face_yaw, face_pitch
            goal[8] = float(m.goalPos[8]) - face_yaw*3.413 - float(m.jointsCenter[8])
            goal[7] = float(m.goalPos[7]) + face_pitch*3.413 - float(m.jointsCenter[7])
            goal[6] = 0.0
            print 'yaw, pitch:',goal[8],goal[7]
            count2 = 0
            print('Detectei face!')
            div = 4.0
            if abs(face_yaw) < 8.0 and abs(face_pitch) < 8.0:
                print 'Te achei!'
                a.play_mp3('fala.mp3')
                e.setExpression('shut')
                m.playMotionFile('gracinha.dat')
                e.setExpression('default')

    count2 = count2 + 1
    #print('count2:', count2)

    if count2 > 150:
        div = 15.0
        if randint(0,100) == 0:
            goal[6] = randint(-1,1)*100
        if randint(0,100) == 0:
            goal[6] = 0
            goal[7] = randint(-1,1)*100
        if randint(0,100) == 0:
            goal[8] = randint(-1,1)*100
        if randint(0,75) == 0:
            if randint(0,15) == 0 and wink == 0:
                e.setExpression('shut')
                wink = 1
            elif wink == 1:
                e.setExpression('default')
                wink = 0

