import sys

# sys.path.insert(0, '/home/luiza/Qiron/backup/beo/')
sys.path.insert(0, '/home/pi/beo/')

from beo import Beo

b = Beo()


class GeraFalas():
    def __init__(self):
        self.file1 = "padrao.mp3"
        self.file2 = "falaBeo.mp3"

    def fala(self, arquivo, frase):
        b.audio.gera_mp3(self.file1, frase)
        b.audio.ajusta_mp3(self.file1, arquivo)
        b.audio.play_mp3(arquivo)
