# coding=utf-8

__author__ = "Guilherme Garcia"
__email__ = "guilesgarcia@gmail.com"

import csv
import requests
from gera_falas import GeraFalas
from operator import attrgetter
from random import randint


class Fala(object):
    def __init__(self, hora, fala, nome_arquivo):
        self.hora = hora
        self.fala = fala
        self.nome_arquivo = nome_arquivo

    def __str__(self):
        return self.nome_arquivo


def gera_falas(falas):
    g = GeraFalas()
    for fala in falas:
        g.fala(frase=fala.fala, arquivo=fala.nome_arquivo)


def main():
    """
    Recebe a lista de alunos do Google Drive e printa os grupos
    """
    lista_falas = list()
    url = 'https://docs.google.com/spreadsheets/d/1F39Z20Jhgo4M9Gdf4Fc23WyOw_wmfQpkMfjoMKVD_lo/export?format=csv'

    response = requests.get(url)
    content = response.content.decode('utf-8')
    reader = csv.DictReader(content.splitlines(), delimiter=',')

    # Dict de alunos
    for a in reader:
        if not a.get('FALA'):
            fala = Fala(hora=a.get('HORA'), fala=a.get('FALA'), nome_arquivo=a.get('NOME_ARQUIVO'))
            lista_falas.append(fala)

    # Gera falas
    gera_falas(lista_falas)


if __name__ == "__main__":
    main()
