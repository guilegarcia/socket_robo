# coding=utf-8

__author__ = "Guilherme Garcia"
__email__ = "guilesgarcia@gmail.com"

import csv
import requests
from operator import attrgetter
from random import randint


class Aluno(object):
    """
    Classe aluno
    """

    def __init__(self, nome, nota=0):
        self.nome = nome
        self.nota = nota

    def __str__(self):
        return self.nome


def gera_numero(inicio, fim, numeros_repetidos):
    """
    Gera um número entre o início e fim que não está nos numeros_repetidos.
    Se não houver número que não seja repetido retorna None
    """
    num = None
    while True:
        num = randint(inicio, fim)
        if num not in numeros_repetidos:
            break
    return num


def gera_grupos(lista_alunos):
    """
    Recebe a lista de aluno e o número de desistentes (para diminuir na geração dos grupos)
    Gera a lista de grupos com alunos com média boa, média, baixa e retorna
    """
    lista_grupos = list()
    grupo = list()
    numeros_gerados = set()  # Registra os números já gerados para não repetir alunos nos grupos
    tam = int(len(lista_alunos) / 3)  # int(round(len(lista_alunos) / 3))  # Quantidade de grupos formados por 3 alunos

    # Lista todos os grupos
    for x in range(0, tam):
        lista_num = list()  # Lista de números dos alunos do grupo
        lista_num.append(gera_numero(0, tam - 1, numeros_gerados))  # Nota boa
        lista_num.append(gera_numero(tam, (tam * 2) - 1, numeros_gerados))  # Nota média
        lista_num.append(gera_numero((tam * 2), (tam * 3) - 1, numeros_gerados))  # Nota baixa

        # Registra cada aluno no grupo
        for num in lista_num:
            # Verifica se há aluno sem grupo (o num retorna None caso não haja mais alunos de um dos 3 grupos)
            if num is not None:
                # Continua o for, pois faltou um aluno para o grupo
                aluno = lista_alunos[num]
                grupo.append(aluno)
                numeros_gerados.add(num)

        # Salva os alunos no grupo
        lista_grupos.append(grupo)
        grupo = []

    # Verifica se sobrou aluno e coloca no primeiro grupo
    for x in range(0, len(lista_alunos)):
        if x not in numeros_gerados:
            aluno = lista_alunos[x]
            lista_grupos[0].append(aluno)

    # Verifica se um grupo está com mais de 4 pessoas e cria novo grupo
    novo_grupo = list()
    for g, grupo in enumerate(lista_grupos):
        if len(grupo) > 4:
            for x in range(0, 2):
                novo_grupo.append(grupo[x])  # Adiciona os 2 aluno novo grupo
                lista_grupos[g].remove(grupo[x])  # Remove o aluno do antigo grupo
            lista_grupos.append(novo_grupo)

    return lista_grupos


def imprime_grupos(grupos):
    """
    Imprime os grupos de alunos
    """
    for x, grupo in enumerate(grupos, 1):
        print('\n__ GRUPO', x, '__')
        for aluno in grupo:
            print(aluno.nome)


def salva_csv(grupos):
    """
    Recebe a lista de grupos e salva em "grupos.csv"
    """
    with open('grupos.csv', 'w') as csvfile:
        fieldnames = ['Grupo', 'Nome']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect='excel')
        writer.writeheader()

        for x, grupo in enumerate(grupos, 1):
            writer.writerow({'Grupo': 'GRUPO ' + str(x), 'Nome': ''})
            for aluno in grupo:
                writer.writerow({'Grupo': '', 'Nome': aluno.nome})


def verifica_duplicados(grupos):
    """
    Verifica se há o mesmo aluno em mais de um grupo
    """
    cont = 0
    for grupo in grupos:
        for aluno in grupo:
            # Verifica se o aluno está em cada grupo
            for g in grupos:
                if g.count(aluno):
                    cont = cont + 1
            if cont > 1:
                print('Repetido: ' + aluno.nome)
            cont = 0


def main():
    """
    Recebe a lista de alunos do Google Drive e printa os grupos
    """
    list_alunos = []
    # Alunos desistentes:
    alunos_desistentes = set()

    url = 'https://docs.google.com/spreadsheets/d/1hTpc4J5LG188qpU9qHtM3hV2u_21mZ0zicUpwN1LT_w/export?format=csv&gid=2062231822'

    response = requests.get(url)
    content = response.content.decode('utf-8')
    reader = csv.DictReader(content.splitlines(), delimiter=',')

    # Dict de alunos
    for a in reader:
        if a['NOME'] not in alunos_desistentes:
            aluno = Aluno(nome=a.get('NOME'))
            if a.get('NOTA'):
                aluno.nota = a.get('NOTA')
            list_alunos.append(aluno)

    # Ordena a lista
    list_alunos = sorted(list_alunos, key=attrgetter('nota'), reverse=True)

    # Recebe os grupos
    grupos = gera_grupos(list_alunos)
    imprime_grupos(grupos)

    # Salva em CSV:
    salva_csv(grupos)


if __name__ == "__main__":
    main()
