import sys
import os
sys.path.append('/home/pi/beo/Modules/Movements')
sys.path.append('/home/pi/beo/Tools/motionEditor')
from Movements import Movements
from Audio import Audio
from motion import Motion
from Wheels import Wheels
#from Eyes import Eyes
import io
import pickle
from threading import Thread
import thread
import time
import json
import numpy as np
from pydub import AudioSegment
from pydub.playback import play
import pygame

pygame.mixer.init()

movements = Movements()
motion = Motion()
wheels = Wheels()
#eyes = Eyes()
audio = Audio()

class Scenario():
	def __init__(self):
		self.filename = ''
		self.wait = 0

	def unicode_to_dict(self, motionList):
		motion = []
		for i in motionList:
			pose = {}
			for j in i.keys():
				pose[int(j)] = i[j]
			motion.append(pose)
		return motion

	def load(self, filename):
		thefile = open(filename, 'r')
		self.filename = os.path.basename(thefile.name)
		print(self.filename)
		return(pickle.load(thefile))
	
	def edit_custom(self, scenario):
		for i, item in enumerate(scenario):
			if(item['action'] == 'custom'):
				type = input('What custom item would you like to input? 1) audio: ')
				if(type == 1):
					type = 'audio'
					file = input('Input the mp3 file location with name: ')
					wait = input('Wait for this to finish before continuing scenario? 1) yes, 0) no: ')
				print("Custom type: ", type, " Path: ", file)
				yesno = input('Is the given data correct? 1) yes, 0) no: ')
				if(yesno):
					scenario[i] = {"data": {"data": file, "wait": wait}, "action": type}
					thefile = open('./scenarios/' + self.filename, 'w')
					pickle.dump(scenario, thefile)
					thefile.close()
				else:
					print("Please start again")
	
	def play_scenario(self, scenario):
		ta = ts = tw = tm = 0
		for i, item in enumerate(scenario):
			if(item['action'] == 'play_motion'):
				tm = Thread(target=self.play_motion, args=(item["data"], 0))
				tm.start()
				print("Playing motion, ", i)
				#thread.start_new_thread(self.play_motion, (item["data"], 0))			
			elif(item['action'] == 'play_speech'):
				ts = Thread(target=self.play_speech, args=(item["data"], 0))
				ts.start()
				print("Playing speech, ", i)
				#thread.start_new_thread(self.play_speech, (item["data"], 0))
			elif(item['action'] == 'rotate_wheels'):
				tw = Thread(target= self.move_wheels, args=(item["data"], 0))
				tw.start()
				print("Rotating wheels", i)
				#thread.start_new_thread(self.move_wheels, (item["data"], 0))
			elif(item['action'] == 'sleep'):
				print("Sleeping", i)
				self.sleep(item["data"])
			elif(item["action"] == "set_expression"):
				self.set_expression(item["data"])
			elif(item["action"] == 'audio'):
				if(ta): ta.join()
				if(tm): tm.join()
				if(ts): ts.join()
				if(tw): tw.join()
				self.wait = item["data"]["wait"]
				#ta = Thread(target=self.play_mp3, args=(item["data"]["data"], 0))
				#ta.start()
				self.play_mp3(item["data"]["data"])

	def play_mp3(self, data):
		pygame.mixer.music.load(data)
		pygame.mixer.music.play(loops=0)
		print("Playing audio")
		if(self.wait):
			while(pygame.mixer.music.get_busy()):
				pass

	def play_motion(self, data, sla):
		motion.keyframes = self.unicode_to_dict(data)
		try:
			movements.play_motion(motion)
		except:
			pass		

	def sleep(self, ms):
		time.sleep(ms/1000)

	def play_speech(self, speech, sla):
		a = np.array(speech, dtype="uint8")
		s = AudioSegment.from_file(io.BytesIO(a), format="mp3")
		play(s)

	def move_wheels(self, data, sla):
		direction = 10
		if(data["direction"] == "forwards"):
			direction = 12
		elif(data["direction"] == "backwards"):
			direction = 8
		print(direction, data['direction'])
		if(data["wheel"] == "left"):
			wheels.move_wheel_left(direction)
		elif(data["wheel"] == "right"):
			wheels.move_wheel_right(direction)
		elif(data["wheel"] == "both"):
			wheels.move(direction)

		time.sleep(data["duration"]/1000)
		try:
			wheels.move(10)
		except:
			pass

	def set_expression(self, expression):
		eyes.set_expression(expression.lower())


#s = Scenario()
#teste = s.load('./scenarios/custom_semsleep.sbeo')
#s.edit_custom(teste)
#teste = s.load('./scenarios/custom_semsleep.sbeo')
#s.play_scenario(teste)
#time.sleep(1)
#movements.disable_all_joints()
