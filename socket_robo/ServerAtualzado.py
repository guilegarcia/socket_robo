import socket
import time


# TODO criar json para enviar as informações em um padrão


def create_file(name):
    '''Create the txt archive to write data'''

    arq = open("%s.txt" % name, "a")
    arq.write("Time(s)          Speed(Km/h)      Distance(m)\n")
    arq.close()


def create_socket(host, port):
    """
    Function for create and return socket instance
    """
    try:
        soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print("Socket criado ")

        soc.bind((host, port))
        print("Servidor online")
        soc.settimeout(1)  # todo testar

        return soc
    except socket.error as msg:
        print('Failed to create socket')


def close_socket(soc):
    soc.close()


def main():
    HOST = ''
    PORT = 8888

    data = ""
    name = input("Nome do usuario: ")
    arq = create_file(name=name)

    startTime = debugTime = time.time()
    totalTime = distTotal = 0

    s = create_socket(host=HOST, port=PORT)

    while 1:
        try:
            data = s.recv(10)
            if data != '':
                rotTime = time.time() - startTime

                totalTime += rotTime
                distTotal += (6.28 * 0.26)

                speed = ((6.28 * 0.26) / rotTime) * 3.6

                arq = open("%s.txt" % name, "a")
                adjust = 17 - len(str("%.2f") % totalTime)
                adjust2 = 17 - len(str("%.2f") % speed)

                arq.write("%.2f%s%.2f%s%.2f\n" % (totalTime, adjust * " ", speed, adjust2 * " ", distTotal))
                print("Speed:%.2f Km/h" % speed)
                startTime = time.time()
                debugTime = time.time()
                arq.close()

        except socket.timeout:
            pass

        if (time.time() - debugTime) >= 3.0:
            debugTime = time.time()
            arq = open("%s.txt" % name, "a")
            arq.write("0.00             0.00             %.2s\n" % distTotal)
            arq.close()

            print("Speed:0.00")
    close_socket(s)


if __name__ == "__main__":
    # execute only if run as a script
    main()
