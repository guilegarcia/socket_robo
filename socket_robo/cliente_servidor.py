import os
import socket

HOST = '192.168.1.202'  # Endereco IP do Servidor
PORT = 8888  # Porta que o Servidor esta

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
dest = (HOST, PORT)


# TODO ver https://stackoverflow.com/questions/46775320/simple-python-server-client-file-transfer

def create_server():  # TODO pode ser que não seja preciso
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    orig = (HOST, PORT)
    udp.bind(orig)
    return udp


def open_motion(filename):
    file = open(os.path.join('motions', filename), 'r')  # todo testar rb ao em vez de b
    file_bytes = bytes(file, 'utf-8')
    return file_bytes


def open_audio(filename):
    file = open(os.path.join('audios', filename), 'rb')
    # file = open('name', 'r')
    return file


def main():
    # Testa mensagem de texto
    msg = input()
    udp.sendto(msg, dest)

    # Testa envio de arquivos
    hi_motion = open_motion("hi.beo")
    udp.sendto(hi_motion, dest)  # TODO testar bytes

    ## recebe mensagens #todo testar receber mensagem da mesma variável udp
    # while True:
    #     msg, cliente = udp.recvfrom(1024)
    #     print (cliente, msg)
    # udp.close()

    udp.close()


if __name__ == "__main__":
    # execute only if run as a script
    main()
