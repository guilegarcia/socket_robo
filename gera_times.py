# coding=utf-8

__author__ = "Guilherme Garcia"
__email__ = "guilesgarcia@gmail.com"

import csv
from operator import attrgetter
from random import randint

import requests

jogadores = [
    {'nome': 'Guile', 'pontos': 5},
    # {'nome': 'Mateus', 'pontos': 5},
    {'nome': 'Rômulo', 'pontos': 5},
    {'nome': 'Marcos', 'pontos': 8},
    # {'nome': 'Vinicius', 'pontos': 7},
    {'nome': 'Flavio', 'pontos': 7},
    {'nome': 'João', 'pontos': 6},
    {'nome': 'Payol', 'pontos': 4},
    {'nome': 'Paulo', 'pontos': 5},
    # {'nome': 'Leo', 'pontos': 3},
    {'nome': 'Dilclei', 'pontos': 5},
    {'nome': 'William (amigo rômulo)', 'pontos': 5},
    {'nome': 'Emanuel', 'pontos': 5},
    # {'nome': 'Cattani', 'pontos': 3},
]

numeros_repetidos_set = set()


def gera_time():
    jogadores_list = list()

    for x in range(5):
        num = gera_numero(0, 9, numeros_repetidos_set)
        numeros_repetidos_set.add(num)
        jogador = jogadores[num]
        jogadores_list.append(jogador)

    return jogadores_list


def gera_pontos_time(time):
    total_pontos = 0
    for jogador in time:
        total_pontos += jogador['pontos']
    return total_pontos


def verifica_pontuacao_times(time1, time2):
    """
    Verifica se os times tem a mesma pontuação, se não tiver returna Falso.
    """
    pontos_time1 = gera_pontos_time(time1)
    pontos_time2 = gera_pontos_time(time2)

    if pontos_time1 == pontos_time2:
        return 0
    else:
        return pontos_time1 - pontos_time2


def gera_numero(inicio, fim, numeros_repetidos):
    """
    Gera um número entre o início e fim que não está nos numeros_repetidos.
    Se não houver número que não seja repetido retorna None
    """
    num = None
    while True:
        num = randint(inicio, fim)
        if num not in numeros_repetidos:
            break
    return num


def print_times(time1, time2):
    print("\nTIME A:")
    for jogador in time1:
        print(jogador['nome'])
    print("\nTIME B:")
    for jogador in time2:
        print(jogador['nome'])


def main():
    """
    Recebe a lista de alunos do Google Drive e printa os grupos
    """
    gerou = False
    time1, time2 = None, None
    while not gerou:
        numeros_repetidos_set.clear()
        time1 = gera_time()
        time2 = gera_time()

        diferenca_pontos = verifica_pontuacao_times(time1, time2)
        if diferenca_pontos == 0:
            gerou = True
            print("PONTOS IGUAIS:")
            print_times(time1, time2)
        elif diferenca_pontos == 1 or diferenca_pontos == -1:
            print("PONTOS 1 ou -1:")
            print_times(time1, time2)




if __name__ == "__main__":
    main()
